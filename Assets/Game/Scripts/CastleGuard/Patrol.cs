using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[TaskCategory("Y-Bot")]
[TaskDescription("NPC patrols a given set of waypoints")]
public class Patrol : MoveToGoal
{
	public CastleGuardMain guard;
	public SharedGameObjectList waypoints;

	public SharedInt wayPointsCompleted;
	private int index = 0;

	public override void OnStart()
	{
		base.OnStart();
		guard = GetComponent<CastleGuardMain>();
		agent.SetDestination(waypoints.Value[index].transform.position);
	}

	public override TaskStatus OnUpdate()
	{
		TaskStatus baseStatus = base.OnUpdate();
		if (baseStatus == TaskStatus.Success)
		{
			index++;
			wayPointsCompleted.Value++;
			if (wayPointsCompleted.Value > 0)
            {
				return TaskStatus.Failure;
            }
			if (index >= waypoints.Value.Count)
			{
				index = 0;
			}
			agent.SetDestination(waypoints.Value[index].transform.position);
		}

		return TaskStatus.Running;
	}
}
