using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[TaskCategory("Y-Bot")]
[TaskDescription("Returns Success if the agent can see the target")]
public class CanSeeTarget : Conditional
{
	public SharedTransform target;
	public SharedFloat distance = 10.0f;
	public SharedFloat fov = 35.0f;

	private Color gizmoColor;

	public override TaskStatus OnUpdate()
	{
		if (target.Value != null)
		{
			Vector3 dirToTarget = target.Value.position - transform.position;
			dirToTarget.y = 0.5f;

			float viewAngle = Mathf.Abs(Vector3.Angle(transform.forward, dirToTarget));
			if (viewAngle < fov.Value * 0.5f)
			{
				Vector3 position = transform.position;
				position.y = 0.5f;

				RaycastHit hit;
				if (Physics.Raycast(position, dirToTarget, out hit, distance.Value))
				{
					if (hit.transform == target.Value)
					{
						gizmoColor = Color.red;
						return TaskStatus.Success;
					}
				}
			}
		}

		gizmoColor = Color.green;
		return TaskStatus.Failure;
	}

	public override void OnDrawGizmos()
	{
#if UNITY_EDITOR
		float halfAngle = fov.Value * 0.5f;
		Vector3 targetDirection = Quaternion.AngleAxis(-halfAngle, transform.up) * transform.forward;
		UnityEditor.Handles.color = gizmoColor;
		UnityEditor.Handles.DrawSolidArc(transform.position, transform.up, targetDirection, fov.Value, distance.Value);
#endif
	}
}
