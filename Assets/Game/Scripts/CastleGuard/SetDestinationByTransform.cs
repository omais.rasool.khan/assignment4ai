using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SetDestinationByTransform : Action
{
    public SharedGameObject targetGameObject;
    public SharedTransform destination;

    // cache the navmeshagent component
    private NavMeshAgent navMeshAgent;
    private GameObject prevGameObject;

    public override void OnStart()
    {
        var currentGameObject = GetDefaultGameObject(targetGameObject.Value);
        if (currentGameObject != prevGameObject)
        {
            navMeshAgent = currentGameObject.GetComponent<NavMeshAgent>();
            prevGameObject = currentGameObject;
        }
    }

    public override TaskStatus OnUpdate()
    {
        if (navMeshAgent == null)
        {
            Debug.LogWarning("NavMeshAgent is null");
            return TaskStatus.Failure;
        }

        return navMeshAgent.SetDestination(destination.Value.position) ? TaskStatus.Success : TaskStatus.Failure;
    }

    public override void OnReset()
    {
        targetGameObject = null;
        destination = null;
    }
}
