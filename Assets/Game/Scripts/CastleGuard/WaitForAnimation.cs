using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[TaskCategory("Y-Bot")]
[TaskDescription("Waits for an animation to be completed")]
public class WaitForAnimation : Action
{
	public SharedString animationName;
	public SharedBool useRootMotion;

	private Animator animator;
	private NavMeshAgent agent;
	private AnimationListener animationListener;
	private UnityAction<int> onAnimationCompletedCallback;
	private UnityAction onAnimatorMoveCallback;

	private bool animationCompleted = false;
	
	private int ANIM_Name;

	public override void OnAwake()
	{
		agent = gameObject.GetComponent<NavMeshAgent>();
		animator = gameObject.GetComponent<Animator>();
		//ANIM_Name = Animator.StringToHash(animationName.Value);
		animationListener = gameObject.GetComponent<AnimationListener>();
		onAnimatorMoveCallback = new UnityAction(OnAnimatorMove);
		onAnimationCompletedCallback = new UnityAction<int>(OnAnimationCompleted);
	}

	public override void OnStart()
	{
		ANIM_Name = Animator.StringToHash(animationName.Value);
		animationCompleted = false;
		animationListener.addAnimationCompletedCallback(ANIM_Name, onAnimationCompletedCallback);

		if (useRootMotion.Value)
		{
			animationListener.addAnimatorMoveListener(onAnimatorMoveCallback);
		}
	}

	private void OnAnimatorMove()
	{
		agent.velocity = animator.deltaPosition / Time.deltaTime;
	}

	private void OnAnimationCompleted(int animName)
	{
		animationCompleted = true;
	}

	public override TaskStatus OnUpdate()
	{
		if (animationCompleted == true)
		{
			return TaskStatus.Success;
		}
		return TaskStatus.Running;
	}

	public override void OnEnd()
	{
		animationListener.removeAnimationCompletedCallback(ANIM_Name, onAnimationCompletedCallback);
		animationListener.removeAnimatorMoveListener(onAnimatorMoveCallback);
	}
}
