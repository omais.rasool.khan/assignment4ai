using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[TaskCategory("Guard-Bot")]
[TaskDescription("Sets Min Position from click Points")]
public class SetMinPoint : Action
{
    public SharedVector3 destination;
    public SharedInt idleIndex;

    

    

    private CastleGuardMain guard;
    public override void OnStart()
    {
        guard = GetComponent<CastleGuardMain>();

        
    }

    public override TaskStatus OnUpdate()
    {
        if (guard.clickedPoints.Count > 0)
        {
            Vector3 selfPos = transform.position;
            float distance = float.MaxValue;
            foreach (Vector3 point in guard.clickedPoints)
            {
                float dist = Vector3.Distance(selfPos, point);
                if (dist < distance)
                {
                    distance = dist;
                    destination.Value = point;
                }
            }
        }

        if (destination != null)
        {
            
            //animationName.Value = animationNames[idleIndex.Value - 1];
            //animationName.Value = "victory_idle";
            //guard.clickedPoints.Remove(destination.Value);
            //guard.clickCount.Value -= 1;
            return TaskStatus.Success;
        }
        else
            return TaskStatus.Failure;
    }
}
