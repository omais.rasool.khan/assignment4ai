using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleGuardMain : MonoBehaviour
{
    public float _health = 100.0f;
    public SharedFloat health;
    public GameObject redArea;

    public List<Vector3> clickedPoints;

    public int idleIndex = 0;

    public SharedInt clickCount;

    private BehaviorTree tree;
    private Bounds bounds;


    // Start is called before the first frame update
    void Start()
    {
        clickedPoints = new List<Vector3>();
        tree = GetComponent<BehaviorTree>();
        clickCount = tree.GetVariable("clicksRegistered") as SharedInt;
        health = tree.GetVariable("health") as SharedFloat;

        Collider collider = redArea.GetComponent<Collider>();

        bounds = collider.bounds;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (clickedPoints.Count < 5)
                {
                    clickedPoints.Add(hit.point);
                    clickCount.Value = clickedPoints.Count;
                }
            }
        }

        if (bounds.Contains(transform.position))
        {
            _health -= 20 * Time.deltaTime;
            health.Value = _health;
        }
    }

}
