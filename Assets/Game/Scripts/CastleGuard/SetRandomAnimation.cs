using BehaviorDesigner.Runtime.Tasks;
using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRandomAnimation : Action
{
    public SharedString animationTriggerName;
    public SharedString animationName;

    public SharedInt wayPointsCompleted;

    private List<string> animationNames;
    private Dictionary<string, string> animations;


    public override void OnStart()
    {
        animationNames = new List<string>();
        animations = new Dictionary<string, string>();

        animationNames.Add("idle1");
        animationNames.Add("idle2");
        animationNames.Add("idle3");

        animations.Add("idle1", "victory_idle");
        animations.Add("idle2", "sad_idle");
        animations.Add("idle3", "warrior_idle");
    }

    public override TaskStatus OnUpdate()
    {
        int animationIndex = Random.Range(1, 3);
        animationTriggerName.Value = animationNames[animationIndex - 1];
        animationName.Value = animations[animationNames[animationIndex - 1]];

        if (animationTriggerName.Value != null && animationName.Value != null)
        {
            wayPointsCompleted.Value = 0;
            return TaskStatus.Success;
        }
            

        else
            return TaskStatus.Failure;
    }
}
