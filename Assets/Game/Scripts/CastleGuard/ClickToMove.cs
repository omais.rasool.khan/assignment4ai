using BehaviorDesigner.Runtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BehaviorTree))]
public class ClickToMove : MonoBehaviour
{
    private BehaviorTree tree;
    private SharedBool onMouseClicked;
    private SharedVector3 destination;

    void Start()
    {
        tree = GetComponent<BehaviorTree>();
        
        onMouseClicked = tree.GetVariable("onMouseClicked") as SharedBool;
        Debug.Assert(onMouseClicked != null, $"{gameObject.name}'s behaviour tree is missing the onMouseClicked Bool variable");

        destination = tree.GetVariable("destination") as SharedVector3;
        Debug.Assert(destination != null, $"{gameObject.name}'s behaviour tree is missing the destination Vector3 variable");
    }

    void Update()
    {
        onMouseClicked.Value = false;
        if (Input.GetMouseButton(0))
		{
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000))
			{
                onMouseClicked.Value = true;
                destination.Value = hit.point;
			}
		}
    }
}
